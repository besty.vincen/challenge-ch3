import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

public class Read implements FileHandling{
    Vector<Integer> nilai = new Vector<Integer>();
    File file;

    public Read(File file) {
        this.file=file;
    }

    @Override
    public Vector<Integer> readFile(String path) throws IOException {
        int total = 0;


        try {
            File file = new File(path);
            if (file.exists()) {

                FileReader reader = new FileReader(file);
                BufferedReader br = new BufferedReader(reader);

                String[] data;
                String line = "";

                while ((line = br.readLine())!=null) {
                    data = line.split(";");

                    for (int i = 1; i < data.length; i++) {
                        nilai.add(Integer.parseInt(data[i]));
                    }
                }
                br.close();

                Main.mainmenu(new Scanner("null"));

            }else {

                Main.gaexist();

            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            throw new IOException(e);
        }
        return nilai;
    }


}
