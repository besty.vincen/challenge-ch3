import java.util.Collections;
import java.util.Vector;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class HitungMedian extends KalkulasiStatistika{
   static Vector<Integer> nilai = new Vector<Integer>();
    public HitungMedian(Vector<Integer> nilai) {
        // TODO Auto-generated constructor stub
        this.nilai = nilai;
    }
    @Override
    public Double getResult() {
        // TODO Auto-generated method stub
        Collections.sort(nilai);
        int n = nilai.size();
        double[] array = new double[n];

        for (int i = 0; i < n; i++) {
            array[i] = nilai.get(i);
        }


        if(n%2==1)
        {
            return DoubleStream.of(array).toArray()[(n+1)/(2-1)];
        }
        else
        {
            return (DoubleStream.of(array).toArray()[(n/2-1)] + DoubleStream.of(array).toArray()[(n/2)])/2;
        }
    }


}
