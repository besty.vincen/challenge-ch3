import java.io.IOException;

import java.util.Vector;

public interface FileHandling {

   Vector<Integer> readFile(String path) throws IOException;

}
