import java.util.Collections;
import java.util.TreeMap;
import java.util.Vector;
import java.util.stream.Collectors;

public class HitungModus extends KalkulasiStatistika{
    double modus;
    Integer mode;
    Vector<Integer> num = new Vector<>();
    public HitungModus(Vector<Integer> num) {
        // TODO Auto-generated constructor stub
        this.num = num;

    }

    @Override
    public Double getResult() {
        // TODO Auto-generated method stub
        modus = Double.valueOf(mode);
        Collections.sort(num);
        mode = num.stream()
                .collect(Collectors.groupingBy(i -> i, () -> new TreeMap<Integer, Long>(), Collectors.counting()))
                .entrySet().stream().sorted((a, b) -> {
                    if (!a.getValue().equals(b.getValue()))
                        return b.getValue().compareTo(a.getValue());
                    return a.getKey().compareTo(b.getKey());
                }).findFirst().get().getKey();
        return modus;
    }


    public String getCount(Vector<Integer> nilai, int i) {
        int count = 0;
        for (int j = 0; j < nilai.size(); j++) {
            if (nilai.get(j) == i) count++;
        }

        return Integer.toString(count);
    }


    public String getElse(Vector<Integer> nilai) {
        int count = 0;
        for (int j = 0; j < nilai.size(); j++) {
            if (nilai.get(j) <6) count++;
        }

        return Integer.toString(count);
    }
}
