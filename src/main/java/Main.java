import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.Vector;


public class Main {
    FileHandling fileHandling;
    static Scanner sc = new Scanner(System.in);
    static Vector<Integer> nilai = new Vector<Integer>();

    public static void main(String[] args) throws IOException {
        // TODO Auto-generated method stub
        new Main();

    }

    public Main() throws IOException {
        // TODO Auto-generated constructor stub
        fileHandling.readFile("C:/temp/direktori/data_sekolah.csv");

    }

    static void mainmenu(Scanner sc) throws IOException {
        // TODO Auto-generated method stub
        System.out.println("--------------------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("--------------------------------");
        System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori");
        System.out.println("berikut: C://temp/direktori");

        System.out.println("Pilih menu:");
        System.out.println("1 Generate txt untuk menampilkan modus");
        System.out.println("2 Generate txt untuk menampilkan nilai rata-rata, median");
        System.out.println("3 Generate kedua file");
        System.out.println("0 Exit");
        System.out.print(">>");



        try {
            int input = sc.nextInt();
            switch (input) {
                case 1:
                    generateModus();
                    mainmenu(new Scanner("null"));
                    break;

                case 2:
                    generateMeanMedian();
                    mainmenu(new Scanner("null"));
                    break;

                case 3:
                    generate2();
                    mainmenu(new Scanner("null"));
                    break;

                case 0:
                    System.exit(0);
            }
        } catch (Exception e) {
            // TODO: handle exception
            throw new IOException(e);

        }
    }

    private static void generateMeanMedian() throws IOException {
        // TODO Auto-generated method stub
        HitungMedian med = new HitungMedian(nilai);
        HitungMean mean = new HitungMean(nilai);
        try {
            File file = new File("C://temp/direktori/data_sekolah_mean_median.txt");
            if (file.createNewFile()) {
                System.out.println("New file has been created!");
            }

            FileWriter writer = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(writer);
            bw.write(meanMedian(med, mean));
            bw.flush();
            bw.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new IOException(e);
        }

    }

    private static String meanMedian(HitungMedian median, HitungMean mean) {
        // TODO Auto-generated method stub
        StringBuffer data = new StringBuffer("");
        data.append("Median = " + median.getResult()).append("\n")
                .append("Mean = " + mean.getResult());
        return data.toString();
    }

    private static void generate2() throws IOException {
        // TODO Auto-generated method stub
        generateMeanMedian();
        generateModus2();

    }

    private static void generateModus() throws IOException {
        // TODO Auto-generated method stub
        HitungModus mod = new HitungModus(nilai);

        try {
            File file = new File("C:/temp/direktori/data_sekolah_Modus.txt");
            if (file.createNewFile()) {
                System.out.println("New file has been created!");
            }

            FileWriter writer = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(writer);
            bw.write(Modus(mod));
            bw.flush();
            bw.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new IOException(e);
        }

    }

    private static String Modus(HitungModus modus) {
        // TODO Auto-generated method stub
        StringBuffer data = new StringBuffer("");
        data.append("Nilai kurang dari 6 || " + modus.getElse(nilai))
                .append("\n").append("6                   || " + modus.getCount(nilai, 6))
                .append("\n").append("7                   || " + modus.getCount(nilai, 7))
                .append("\n").append("8                   || " + modus.getCount(nilai, 8))
                .append("\n").append("9                   || " + modus.getCount(nilai, 9))
                .append("\n").append("10                  || " + modus.getCount(nilai, 10));
        return data.toString();
    }

    private static void generateModus2() {
        // TODO Auto-generated method stub
        HitungModus mod = new HitungModus(nilai);

        try {
            File file = new File("C:/temp/direktori/data_sekolah_Modus.txt");
            if (file.createNewFile()) {
                System.out.println("New file has been created!");
            }

            FileWriter writer = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(writer);
            bw.write(Modus2(mod));
            bw.flush();
            bw.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    private static String Modus2(HitungModus modus) {
        // TODO Auto-generated method stub
        StringBuffer data = new StringBuffer("");
        data.append("Modus = " + modus.getResult());
        return data.toString();
    }


    static void gaexist() throws IOException {
        // TODO Auto-generated method stub
        System.out.println("--------------------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("--------------------------------");
        System.out.println("File tidak ditemukan");

        System.out.println("0 Exit");
        System.out.println("1 Kembali ke menu utama");
        System.out.print(">>");



        try {
            int input = sc.nextInt();
            switch (input) {
                case 1:
                    mainmenu(new Scanner("null"));
                    break;

                case 0:
                    System.exit(0);
            }
        } catch (Exception e) {
            // TODO: handle exception
            throw new IOException(e);
        }
    }

    private void sukses() throws IOException {
        // TODO Auto-generated method stub
        System.out.println("--------------------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("--------------------------------");
        System.out.println("File telah di generate di C://temp/direktori");
        System.out.println("Silahkan cek");

        System.out.println("0 Exit");
        System.out.println("1 Kembali ke menu utama");
        System.out.print(">>");

        try {
            int input = sc.nextInt();
            switch (input) {
                case 1:
                    mainmenu(new Scanner("null"));
                    break;

                case 0:
                    System.exit(0);
            }
        } catch (Exception e) {
            // TODO: handle exception
            throw new IOException(e);

        }

    }

}
