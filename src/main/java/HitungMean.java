import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

public class HitungMean extends KalkulasiStatistika{
    double mean;
    Vector<Integer> num = new Vector<>();

    public HitungMean(Vector<Integer> num) {
        // TODO Auto-generated constructor stub
        this.num = num;
    }


    @Override
    public Double getResult() {
        // TODO Auto-generated method stub
        List<Integer> nilaiMurid = num.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());

        mean = nilaiMurid.stream().reduce(0, Integer::sum) / (int) nilaiMurid.stream().count();
        return mean;
    }

}
