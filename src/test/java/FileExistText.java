import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Vector;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FileExistText {

    File file = new File("C:/temp/direktori/data_sekolah.csv");


    FileHandling fileHandling;

    @BeforeEach
    void init() {
        // inisialisasi mock cara kedua
        fileHandling = new Read(file);
    }

    @Test
    void readFile_Success() {
        Integer input = 0;
        String path = "C:/temp/direktori/data_sekolah.csv";
        Vector<Integer> resultList = Assertions.assertDoesNotThrow(()->fileHandling.readFile(path));



        InputStream in = new ByteArrayInputStream(new byte[]{input.byteValue()});
        System.setIn(in);
        Assertions.assertEquals(198, resultList.size());
    }

    @Test
    void readFile_Failed_FileNotFound() {
        String path = "C:/WrongFile";
        Assertions.assertThrows(IOException.class, () -> fileHandling.readFile(path));
    }

    @Test
    void menu_Exit() throws IOException {
        String input = "0";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        Main.mainmenu(new Scanner(input));
    }

    @Test
    void menu_Null(){
        Assertions.assertThrows(InputMismatchException.class, () -> Main.mainmenu(new Scanner("null")));
    }

}